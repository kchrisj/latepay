<?php
namespace Apps;

class LatePayerInvoiceSender
{

  static function sendLatePayerInvoice($dbConnection, $latePayersInvoiceArray){
    foreach($latePayersInvoiceArray as $invoiceId){
        try {
            $useDetailsObject = EquipmentUseDAO::getEquipmentUseDetails($dbConnection, $invoiceId);
            //print_r($useDetailsObject);
          } catch (Exception $e) {
            $log->error($e->getMessage());
            echo "Problem retrieving data from database\r\n";
            die();
        }
        try{
              //print_r($useDetailsObject);
              $emailMessage = EmailMessageGenerator::createEmail($useDetailsObject);
            } catch(Exception $e){
              $log->error($e->getMessage());
              echo "Could not generate email message\r\n";
              die();
        }
        try{
              //print_r($emailMessage);
              EmailMessageSender::sendEmail($emailMessage);
           } catch(Exception $e){
              $log->error($e->getMessage());
              echo "Could not send email message\r\n";
              die();
        }
    }
  }
}
