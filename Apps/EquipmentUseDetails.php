<?php
namespace Apps;
/**
*  Bill information object
*
*/
class EquipmentUseDetails {

  public $invoiceDetailsArray = [];
  public $serviceInfoArray = [];
  public $attachmentArray = [];
  public $invoiceNumber;
  public $issueDate;
  public $dueDate;
  public $serviceId;
  public $serviceTime;
  public $quantity;
  public $rate;
  public $total;
  public $fileName;
  public $payer;
  public $email;
  public $userName;
  public $daysOver;

  public function __construct() {
    /*$this -> serviceInfoRArray = $serviceInfoArray = [];
    $this -> attachmentArray = $attachmentArray = [];
    $this -> id = $id;
    $this -> invoiceNumber = $invoiceNumber;
    $this -> details = $details;
    $this -> service_id = $service_id;
    $this -> total = $total;
    */

  }

}
