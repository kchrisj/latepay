<?php
namespace Apps;

class LatePayersDAO
{
  function getLatePayers($DBConnection)
  {
    $lateInvoices = [];
    $dateQuery = "SELECT CI.*
    FROM core_invoice as CI
    WHERE DATEDIFF(CURRENT_DATE, due) > 30 AND DATEDIFF(CURRENT_DATE, due) < 60";

    if($results = $DBConnection->query($dateQuery)){
      while($row = $results->fetch_assoc()) {
        //echo $row["number"];
        $lateInvoices[] = $row["number"];
      }
       return $lateInvoices;
    }
  }
}
