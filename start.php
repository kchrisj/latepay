<?php
require_once(__DIR__ .'/vendor/autoload.php');

use \Apps\DBConnect;
use \Apps\LatePayersDAO;
use \Apps\LatePayerInvoiceSender;

use \Apps\EquipmentUseDAO;
use \Apps\EquipmentUseDetails;

use \Apps\EmailMessageGenerator;
use \Apps\EmailMessageSender;
use \Apps\InvoiceSender;
use \Apps\TemplateView;
use \Apps\FileCreator;

use Monolog\Logger;
use Monolog\Handler\StreamHandler;

// create a log channel
$log = new Logger('bills');
$dbStream = new StreamHandler('data/billing.log', Logger::ERROR);
$log->pushHandler($dbStream);

try {
    $dbConnection = DBConnect::getConnection();
    } catch (Exception $e) {
    echo "Problem connecting to the database\r\n";
    die();
}

try {
        $latePayersInvoiceArray = LatePayersDAO::GETLatePayers($dbConnection);
    } catch (Exception $e) {
        $log->error($e->getMessage());
        echo "Problem late payer class\r\n";
        die();
}

//print_r($latePayersInvoiceArray);
try {
        LatePayerInvoiceSender::sendLatePayerInvoice($dbConnection, $latePayersInvoiceArray);
    } catch (Exception $e) {
        $log->error($e->getMessage());
        echo "Problem LatePayerInvoiceSender class\r\n";
        die();
}
