#  Past Due Invoice

Third part of a four part system:
1. Send current invoice via email to P.I. (Invoice Sender)
2. P.I. can view details of current invoice via a web page (invoiceDetialsVue)
3. Send past due invoice via email to P.I. (latePay)
4. P.I. can view details of past due invoice via a web page (latePayInvoiceDetailsVue)

This is part three
    --> Email Past Due invoice to P.I.

Main files:
1.  latePay/
    start.php

2.  latePay/Apps/
    --> LatePayersDAO.php
        --> Query database based on invoice number

3.  latePay/Apps/
    --> LatePayerInvoiceSender
        --> create and send email to P.I.
